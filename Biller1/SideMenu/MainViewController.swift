//
//  MainViewController.swift
//  Biller1
//
//  Created by Harshit Maheshwari on 28/01/19.
//  Copyright © 2019 Harshit Maheshwari. All rights reserved.
//

import UIKit
import LGSideMenuController

class MainViewController: LGSideMenuController {

   private var type: UInt?
    override func viewDidLoad() {
        super.viewDidLoad()
         NotificationCenter.default.addObserver(self, selector: #selector(dismissToRootVC), name: NSNotification.Name(rawValue: "dismissToRootVC"), object: nil)
  self.setup(type: 0)
        // Do any additional setup after loading the view.
    }
    
    @objc func dismissToRootVC(){
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    func setup(type: UInt) {
        self.type = type
        
        // -----
        
        if (self.storyboard != nil) {
            // Left and Right view controllers is set in storyboard
            // Use custom segues with class "LGSideMenuSegue" and identifiers "left" and "right"
            
            // Sizes and styles is set in storybord
            // You can also find there all other properties
            
            // LGSideMenuController fully customizable from storyboard
        }
        else {
            leftViewController = LeftViewController()
            //rightViewController = RightViewController()
            
            leftViewWidth = 280.0;
            leftViewBackgroundImage = UIImage(named: "MenuBackground")
            leftViewBackgroundColor = UIColor(red: 0.5, green: 0.65, blue: 0.5, alpha: 0.95)
            rootViewCoverColorForLeftView = UIColor(red: 0.0, green: 1.0, blue: 0.0, alpha: 0.05)
            
            rightViewWidth = 100.0;
        }
        
        // -----
        
        let greenCoverColor = UIColor(red: 0.0, green: 0.1, blue: 0.0, alpha: 0.3)
        let purpleCoverColor = UIColor(red: 0.1, green: 0.0, blue: 0.1, alpha: 0.3)
        let regularStyle: UIBlurEffect.Style
        
        if #available(iOS 10.0, *) {
            regularStyle = .regular
        }
        else {
            regularStyle = .light
        }
        
        // -----
        
        switch type {
        case 0:
            leftViewPresentationStyle = .scaleFromBig
            
            
            break
        case 1:
            leftViewPresentationStyle = .slideAbove
            rootViewCoverColorForLeftView = greenCoverColor
            
            rightViewPresentationStyle = .slideAbove
            rootViewCoverColorForRightView = purpleCoverColor
            
            break
        case 2:
            leftViewPresentationStyle = .slideBelow
            
            
            break
        case 3:
            leftViewPresentationStyle = .scaleFromLittle
            
            
            break
        case 4:
            leftViewPresentationStyle = .scaleFromBig
            rootViewCoverBlurEffectForLeftView = UIBlurEffect(style: regularStyle)
            rootViewCoverAlphaForLeftView = 0.8
            
            rightViewPresentationStyle = .scaleFromBig
            rootViewCoverBlurEffectForRightView = UIBlurEffect(style: regularStyle)
            rootViewCoverAlphaForRightView = 0.8
            
            break
        case 5:
            leftViewPresentationStyle = .scaleFromBig
            leftViewCoverBlurEffect = UIBlurEffect(style: .dark)
            leftViewCoverColor = nil
            
            
            break
        case 6:
            leftViewPresentationStyle = .slideAbove
            leftViewBackgroundBlurEffect = UIBlurEffect(style: regularStyle)
            leftViewBackgroundColor = UIColor(red: 0.0, green: 1.0, blue: 0.0, alpha: 0.05)
            rootViewCoverColorForLeftView = greenCoverColor
            
         
            break
        case 7:
            leftViewPresentationStyle = .slideAbove
            rootViewCoverColorForLeftView = greenCoverColor
            
           
            
            break
        case 8:
            leftViewPresentationStyle = .scaleFromBig
            leftViewStatusBarStyle = .lightContent
            
           
            
            break
        case 9:
            swipeGestureArea = .full
            
            leftViewPresentationStyle = .scaleFromBig
           
            
            break
        case 10:
            leftViewPresentationStyle = .scaleFromBig
           
            
            break
        case 11:
            rootViewLayerBorderWidth = 5.0
            rootViewLayerBorderColor = .white
            rootViewLayerShadowRadius = 10.0
            
            leftViewSwipeGestureRange = LGSideMenuSwipeGestureRangeMake(0.0, 88.0)
            leftViewPresentationStyle = .scaleFromBig
            leftViewAnimationDuration = 1.0
            leftViewBackgroundColor = UIColor(red: 0.5, green: 0.75, blue: 0.5, alpha: 1.0)
            leftViewBackgroundImageInitialScale = 1.5
            leftViewInitialOffsetX = -200.0
            leftViewInitialScale = 1.5
            leftViewCoverBlurEffect = UIBlurEffect(style: .dark)
            leftViewBackgroundImage = nil;
            
            rootViewScaleForLeftView = 0.6
            rootViewCoverColorForLeftView = UIColor(red: 1.0, green: 1.0, blue: 0.0, alpha: 0.3)
            rootViewCoverBlurEffectForLeftView = UIBlurEffect(style: regularStyle)
            rootViewCoverAlphaForLeftView = 0.9
            
            
            rootViewCoverColorForRightView = UIColor(red: 0.0, green: 0.5, blue: 1.0, alpha: 0.3)
            rootViewCoverBlurEffectForRightView = UIBlurEffect(style: regularStyle)
            rootViewCoverAlphaForRightView = 0.9
            
            
            break
        default:
            break
        }
    }
    
    override func leftViewWillLayoutSubviews(with size: CGSize) {
        super.leftViewWillLayoutSubviews(with: size)
        
        if !isLeftViewStatusBarHidden {
            leftView?.frame = CGRect(x: 0.0, y: 20.0, width: size.width, height: size.height - 20.0)
        }
    }
    
    override func rightViewWillLayoutSubviews(with size: CGSize) {
        super.rightViewWillLayoutSubviews(with: size)
        
        if (!isRightViewStatusBarHidden ||
            (rightViewAlwaysVisibleOptions.contains(.onPadLandscape) &&
                UI_USER_INTERFACE_IDIOM() == .pad &&
                UIInterfaceOrientation.landscapeLeft.isLandscape)) {
            rightView?.frame = CGRect(x: 0.0, y: 20.0, width: size.width, height: size.height - 20.0)
        }
    }
    
    override var isLeftViewStatusBarHidden: Bool {
        get {
            if (type == 8) {
                return UIInterfaceOrientation.landscapeLeft.isLandscape
            }
            
            return super.isLeftViewStatusBarHidden
        }
        
        set {
            super.isLeftViewStatusBarHidden = newValue
        }
    }
    

    
    deinit {
        print("MainViewController deinitialized")
    }

    
}

    


