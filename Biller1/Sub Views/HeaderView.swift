//
//  HeaderView.swift
//  Biller1
//
//  Created by Harshit Maheshwari on 09/03/19.
//  Copyright © 2019 Harshit Maheshwari. All rights reserved.
//

import UIKit

class HeaderView: UITableViewHeaderFooterView {

    @IBOutlet weak var name: UILabel!
    
}
