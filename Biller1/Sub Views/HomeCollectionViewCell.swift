import UIKit

class HomeCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var tableNumber: UILabel!
    @IBOutlet weak var tableName: UILabel!
    
}
