import Foundation
import UIKit

class BillTableCell:UITableViewCell{
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var amount: UILabel!
    @IBOutlet weak var quantity: UILabel!
    @IBOutlet weak var rate: UILabel!
    
}
