import UIKit

class ForgetPasswordVC: UIViewController {

    @IBOutlet weak var ForgetEmail: UIView!
    @IBOutlet weak var HomeView: UIView!
    
    @IBOutlet weak var bottomHeight: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        
        self.HomeView.layer.shadowColor = UIColor.lightGray.cgColor
        self.HomeView.layer.shadowOpacity = 0.5
        self.HomeView.layer.shadowOffset = CGSize.zero
        self.HomeView.layer.shadowRadius = 10
        self.HomeView.layer.masksToBounds = false
        
        self.ForgetEmail.layer.borderWidth = 1
        self.ForgetEmail.layer.borderColor = UIColor.lightGray.cgColor
        
             
        let screenRect = UIScreen.main.bounds
        let coverView = UIView(frame: screenRect)
        coverView.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        
    }


}
