import UIKit
import MBProgressHUD

class LoginVC: UIViewController {
    @IBOutlet weak var LoginView: UIView!
    @IBOutlet weak var EmailView: UIView!
    @IBOutlet weak var PasswordView: UIView!
    @IBOutlet weak var ForgetPasswordView: UIView!
    @IBOutlet weak var LogInLbl: UILabel!
    @IBOutlet weak var emailTxtField: UITextField!
    @IBOutlet weak var LinkSentView: UIView!
    @IBOutlet weak var EmailSendLinkView: UIView!
    @IBOutlet weak var showPassword: UIButton!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    
    var isshowpass = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        showPassword.isHidden = false
        LogInLbl.isHidden = false
        ForgetPasswordView.isHidden = true
        LinkSentView.isHidden = true
        LoginView.isHidden = false
       self.hideKeyboardWhenTappedAround()

        self.LoginView.layer.shadowColor = UIColor.lightGray.cgColor
        self.LoginView.layer.shadowOpacity = 0.5
        self.LoginView.layer.shadowOffset = CGSize.zero
        self.LoginView.layer.shadowRadius = 10
        self.LoginView.layer.masksToBounds = false
        
        self.EmailView.layer.borderWidth = 1
        self.EmailView.layer.borderColor = UIColor.lightGray.cgColor
        
        self.PasswordView.layer.borderWidth = 1
        self.PasswordView.layer.borderColor = UIColor.lightGray.cgColor
        
        self.EmailSendLinkView.layer.borderWidth = 1
        self.EmailSendLinkView.layer.borderColor = UIColor.lightGray.cgColor
        

    }
 
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    @IBAction func ForgetPasswordBtn(_ sender: Any) {
        if ForgetPasswordView.isHidden {
            ForgetPasswordView.isHidden = false
            LogInLbl.isHidden = true
        } else {
            ForgetPasswordView.isHidden = true
            LogInLbl.isHidden = false

        }
        self.ForgetPasswordView.transform = CGAffineTransform(scaleX: 0, y: 0)
        UIView.animate(withDuration: 0.3, delay: 0.1, usingSpringWithDamping: 1, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {
            self.ForgetPasswordView.transform = .identity
        }) { (comp) in
            //self.ForgetPasswordView.isHidden = true
          //  self.LogInLbl.isHidden = false
        }
        
    }
    
    @IBAction func showPass(_ sender: Any) {
        
        if(!isshowpass){
            self.password.isSecureTextEntry = false
            
            (sender as AnyObject).setImage(#imageLiteral(resourceName: "HidePassword"), for: UIControl.State.normal)
            isshowpass = true
        }else{
            self.password.isSecureTextEntry = true
            
            (sender as AnyObject).setImage(#imageLiteral(resourceName: "showPassword"), for: UIControl.State.normal)
            isshowpass = false
        }
        
    }
    
    @IBAction func SendLinkBtn(_ sender: Any) {
        
        
        if(!DataValidation.isValidEmail(testStr: emailTxtField.text!)){
            
            self.showToast(message: "Invalid email")
            return
        }
        
        if (emailTxtField.text?.isEmpty ?? nil)!{
            let alert = UIAlertController(title: "Sorry", message: "Please enter a valid email address", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        else {
            LinkSentView.isHidden = false

        }
        
        
        
        
        guard  let email = emailTxtField.text else {
            return
        }
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        let parameters = [
            "email": email,
            ] as [String : Any]

        WebServices.requestHttp(partURL: "auth/forgot-password", parameters: parameters, method: HttpsMethod.Post, decode: { json -> ForgotPassword? in
            
            guard let response = json as? ForgotPassword else{
                return nil
            }
            
            return response
            
        }) { (response:Result) in
            DispatchQueue.main.async {
                MBProgressHUD.hide(for: self.view, animated: true)
                switch response{
                case .success(let data) :
                    
                    
                    
                   self.LinkSentView.isHidden = false
                    self.ForgetPasswordView.isHidden = true
                   self.LoginView.isHidden = true
                    
                    break
                    
                case .failure(let error):
                    print(error.localizedDescription)
                    
                  
                    
                    break
                    
                }
                
                
            }
            
        }

       
      
    }
    
    @IBAction func LogInBtn(_ sender: Any) {
        
    
    }
    
    
    
    @IBAction func btnLoginUser(_ sender: Any) {
        
        if(!DataValidation.isValidEmail(testStr: emailTextField.text!)){
            
            self.showToast(message: "Invalid email")
            return
        }
        
        if(password.text != nil && password.text!.count < 1){
            
            self.showToast(message: "Enter Password")
            return
        }
        
        
        self.view.endEditing(true)
        guard  let password = password.text else {
            return
        }
        
        guard  let email = emailTextField.text else {
            return
        }
        let parameters = [
            "email": email,
            "password": password
            ] as [String : Any]
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        WebServices.requestHttp(partURL: "auth/login", parameters: parameters, method: HttpsMethod.Post, decode: { json -> UserInfo? in
            
            guard let response = json as? UserInfo else{
                return nil
            }
            
            return response
            
        }) { (response:Result) in
            DispatchQueue.main.async {
                MBProgressHUD.hide(for: self.view, animated: true)
                
            
                switch(response){
                    
               
                case .success(let data):
                    
                    UserData.userInfo = data
                    
                    let encodedData: Data = try! JSONEncoder.init().encode(UserData.userInfo)
                    UserDefaults.standard.set(encodedData, forKey: "userInfo")
                    UserDefaults.standard.synchronize()

                    self.performSegue(withIdentifier: "toHomeFromLogin", sender: data)
                    break
                case .failure(let error):
                    print(error.localizedDescription)
                }
                
                
                }
            
        }
        
       
    }
    
    @IBAction func CancelBtn(_ sender: Any) {
        
       
        if ForgetPasswordView.isHidden {
            ForgetPasswordView.isHidden = false
            
//            self.ForgetPasswordView.transform = CGAffineTransform(scaleX: 0, y: 0)
//            UIView.animate(withDuration: 0.5, delay: 0.5, usingSpringWithDamping: 1, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {
//                self.ForgetPasswordView.transform = .identity
//            }, completion: nil)
         
        } else {
            self.ForgetPasswordView.isHidden = true
            self.LogInLbl.isHidden = false
            
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
       // let childClass = segue.destination as! ViewController
       // childClass.userInfo = sender as? UserInfo
    }

}

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}

