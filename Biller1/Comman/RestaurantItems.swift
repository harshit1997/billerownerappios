import Foundation

struct Items: Codable {
    let items: [WelcomeItem]
    let count, pages: Int
}

struct WelcomeItem: Codable {
    let id: String?
    let noOfPersons, paymentMethod: Int?
    let incomingorderRefID: String?
    let channel: Channel?
    let isActive: Bool?
    let category, total: Int?
    let isReviewed: Bool?
    let archiveComment: String?
    let serivceTax: Int?
    let totalTax,totalDiscount: Float?
    let invoiceNumber: String?
    let billCount, orderNumber: Int?
    let ageGroup: String?
    let discountType: Int?
    let couponID: String?
    let groupID: Int?
    let items : [KotItemElement]?
    let kotItems : [KotItemElement]?
    let tableNumber: String?
    let user_id: UserID?
    let restaurantID: String?
    let serveID: String?
    let customItems: [JSONAny]?
    let updatedAt: String?
    let totalAmount, cashAmount: Float?
    let itemID: String?

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case noOfPersons = "no_of_persons"
        case paymentMethod = "payment_method"
        case incomingorderRefID = "incomingorder_ref_id"
        case channel
        case isActive = "is_active"
        case category, total
        case isReviewed = "is_reviewed"
        case archiveComment = "archive_comment"
        case totalDiscount = "total_discount"
        case totalTax = "total_tax"
        case serivceTax = "serivce_tax"
        case invoiceNumber = "invoice_number"
        case billCount = "bill_count"
        case orderNumber = "order_number"
        case ageGroup = "age_group"
        case discountType = "discount_type"
        case couponID = "coupon_id"
        case groupID = "group_id"
        case items
        case kotItems = "kot_items"
        case tableNumber = "table_number"
        case user_id = "user_id"
        case restaurantID = "restaurant_id"
        case serveID = "serve_id"
        case customItems = "custom_items"
        case updatedAt = "updated_at"
        case totalAmount = "total_amount"
        case cashAmount = "cash_amount"
        case itemID = "id"
    }
}

enum Channel: String, Codable {
    case biller = "biller"
}

struct KotItemElement: Codable {
    let discountAmount: Int?
    let addons: [JSONAny]?
    let comment, start, finish: String?
    let id: String?
    let itemID: String?
    let quantity: Int?

    enum CodingKeys: String, CodingKey {
        case discountAmount = "discount_amount"
        case addons, comment, start, finish
        case id = "_id"
        case itemID = "item_id"
        case quantity
    }
}



struct UserID: Codable {
    let id: String?
    let restaurantVisited: [String]?
    let restaurantSubscribed: [JSONAny]?
    let email, birthdaydate, birthdaymonth, birthdayyear: String?
    let isMarried, anniversarydate, anniversarymonth, anniversaryyear: String?
    let gender: String?
    let lastVisited: String?
    let name: String?
    let phoneNumber, updatedAt: String?
    let v: Int?

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case restaurantVisited = "restaurant_visited"
        case restaurantSubscribed = "restaurant_subscribed"
        case email, birthdaydate, birthdaymonth, birthdayyear, isMarried, anniversarydate, anniversarymonth, anniversaryyear, gender
        case lastVisited = "last_visited"
        case name
        case phoneNumber = "phone_number"
        case updatedAt = "updated_at"
        case v = "__v"
    }
}


// MARK: Encode/decode helpers

class JSONNull: Codable, Hashable {

    public static func == (lhs: JSONNull, rhs: JSONNull) -> Bool {
        return true
    }

    public var hashValue: Int {
        return 0
    }

    public init() {}

    public required init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if !container.decodeNil() {
            throw DecodingError.typeMismatch(JSONNull.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for JSONNull"))
        }
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encodeNil()
    }
}

class JSONCodingKey: CodingKey {
    let key: String

    required init?(intValue: Int) {
        return nil
    }

    required init?(stringValue: String) {
        key = stringValue
    }

    var intValue: Int? {
        return nil
    }

    var stringValue: String {
        return key
    }
}

class JSONAny: Codable {
    let value: Any

    static func decodingError(forCodingPath codingPath: [CodingKey]) -> DecodingError {
        let context = DecodingError.Context(codingPath: codingPath, debugDescription: "Cannot decode JSONAny")
        return DecodingError.typeMismatch(JSONAny.self, context)
    }

    static func encodingError(forValue value: Any, codingPath: [CodingKey]) -> EncodingError {
        let context = EncodingError.Context(codingPath: codingPath, debugDescription: "Cannot encode JSONAny")
        return EncodingError.invalidValue(value, context)
    }

    static func decode(from container: SingleValueDecodingContainer) throws -> Any {
        if let value = try? container.decode(Bool.self) {
            return value
        }
        if let value = try? container.decode(Int64.self) {
            return value
        }
        if let value = try? container.decode(Double.self) {
            return value
        }
        if let value = try? container.decode(String.self) {
            return value
        }
        if container.decodeNil() {
            return JSONNull()
        }
        throw decodingError(forCodingPath: container.codingPath)
    }

    static func decode(from container: inout UnkeyedDecodingContainer) throws -> Any {
        if let value = try? container.decode(Bool.self) {
            return value
        }
        if let value = try? container.decode(Int64.self) {
            return value
        }
        if let value = try? container.decode(Double.self) {
            return value
        }
        if let value = try? container.decode(String.self) {
            return value
        }
        if let value = try? container.decodeNil() {
            if value {
                return JSONNull()
            }
        }
        if var container = try? container.nestedUnkeyedContainer() {
            return try decodeArray(from: &container)
        }
        if var container = try? container.nestedContainer(keyedBy: JSONCodingKey.self) {
            return try decodeDictionary(from: &container)
        }
        throw decodingError(forCodingPath: container.codingPath)
    }

    static func decode(from container: inout KeyedDecodingContainer<JSONCodingKey>, forKey key: JSONCodingKey) throws -> Any {
        if let value = try? container.decode(Bool.self, forKey: key) {
            return value
        }
        if let value = try? container.decode(Int64.self, forKey: key) {
            return value
        }
        if let value = try? container.decode(Double.self, forKey: key) {
            return value
        }
        if let value = try? container.decode(String.self, forKey: key) {
            return value
        }
        if let value = try? container.decodeNil(forKey: key) {
            if value {
                return JSONNull()
            }
        }
        if var container = try? container.nestedUnkeyedContainer(forKey: key) {
            return try decodeArray(from: &container)
        }
        if var container = try? container.nestedContainer(keyedBy: JSONCodingKey.self, forKey: key) {
            return try decodeDictionary(from: &container)
        }
        throw decodingError(forCodingPath: container.codingPath)
    }

    static func decodeArray(from container: inout UnkeyedDecodingContainer) throws -> [Any] {
        var arr: [Any] = []
        while !container.isAtEnd {
            let value = try decode(from: &container)
            arr.append(value)
        }
        return arr
    }

    static func decodeDictionary(from container: inout KeyedDecodingContainer<JSONCodingKey>) throws -> [String: Any] {
        var dict = [String: Any]()
        for key in container.allKeys {
            let value = try decode(from: &container, forKey: key)
            dict[key.stringValue] = value
        }
        return dict
    }

    static func encode(to container: inout UnkeyedEncodingContainer, array: [Any]) throws {
        for value in array {
            if let value = value as? Bool {
                try container.encode(value)
            } else if let value = value as? Int64 {
                try container.encode(value)
            } else if let value = value as? Double {
                try container.encode(value)
            } else if let value = value as? String {
                try container.encode(value)
            } else if value is JSONNull {
                try container.encodeNil()
            } else if let value = value as? [Any] {
                var container = container.nestedUnkeyedContainer()
                try encode(to: &container, array: value)
            } else if let value = value as? [String: Any] {
                var container = container.nestedContainer(keyedBy: JSONCodingKey.self)
                try encode(to: &container, dictionary: value)
            } else {
                throw encodingError(forValue: value, codingPath: container.codingPath)
            }
        }
    }

    static func encode(to container: inout KeyedEncodingContainer<JSONCodingKey>, dictionary: [String: Any]) throws {
        for (key, value) in dictionary {
            let key = JSONCodingKey(stringValue: key)!
            if let value = value as? Bool {
                try container.encode(value, forKey: key)
            } else if let value = value as? Int64 {
                try container.encode(value, forKey: key)
            } else if let value = value as? Double {
                try container.encode(value, forKey: key)
            } else if let value = value as? String {
                try container.encode(value, forKey: key)
            } else if value is JSONNull {
                try container.encodeNil(forKey: key)
            } else if let value = value as? [Any] {
                var container = container.nestedUnkeyedContainer(forKey: key)
                try encode(to: &container, array: value)
            } else if let value = value as? [String: Any] {
                var container = container.nestedContainer(keyedBy: JSONCodingKey.self, forKey: key)
                try encode(to: &container, dictionary: value)
            } else {
                throw encodingError(forValue: value, codingPath: container.codingPath)
            }
        }
    }

    static func encode(to container: inout SingleValueEncodingContainer, value: Any) throws {
        if let value = value as? Bool {
            try container.encode(value)
        } else if let value = value as? Int64 {
            try container.encode(value)
        } else if let value = value as? Double {
            try container.encode(value)
        } else if let value = value as? String {
            try container.encode(value)
        } else if value is JSONNull {
            try container.encodeNil()
        } else {
            throw encodingError(forValue: value, codingPath: container.codingPath)
        }
    }

    public required init(from decoder: Decoder) throws {
        if var arrayContainer = try? decoder.unkeyedContainer() {
            self.value = try JSONAny.decodeArray(from: &arrayContainer)
        } else if var container = try? decoder.container(keyedBy: JSONCodingKey.self) {
            self.value = try JSONAny.decodeDictionary(from: &container)
        } else {
            let container = try decoder.singleValueContainer()
            self.value = try JSONAny.decode(from: container)
        }
    }

    public func encode(to encoder: Encoder) throws {
        if let arr = self.value as? [Any] {
            var container = encoder.unkeyedContainer()
            try JSONAny.encode(to: &container, array: arr)
        } else if let dict = self.value as? [String: Any] {
            var container = encoder.container(keyedBy: JSONCodingKey.self)
            try JSONAny.encode(to: &container, dictionary: dict)
        } else {
            var container = encoder.singleValueContainer()
            try JSONAny.encode(to: &container, value: self.value)
        }
    }
}












//import Foundation
//
//struct Items: Codable {
//    let items: [WelcomeItem]
//    let count, pages: Int
//}
//
//struct WelcomeItem: Codable {
//    let id: String
//    let noOfPersons: Int
//    let channel: String
//    let statusupdateIDS: [JSONAny]?
//    let paymentMethod: Int
//    let isActive: Bool
//    let category, total: Int
//    let isReviewed: Bool
//    let archiveComment: JSONNull?
//    let totalDiscount, totalTax, serivceTax: Int
//    let invoiceNumber: String?
//    let billCount: Int
//    let orderNumber: Int?
//    let ageGroup: JSONNull?
//    let discountType: Int
//    let couponID: JSONNull?
//    let groupID: Int?
//    let items, kotItems: [KotItemElement]
//    let restaurantID: RestaurantID
//    let serveID: String?
//    let customItems: [JSONAny]
//    let updatedAt: String
//    let createdAt: String?
//    let v: Int
//    let itemID: String
//    let incomingorderRefID: IncomingorderRefID?
//    let userID: UserID?
//    let totalAmount: Int?
//
//    enum CodingKeys: String, CodingKey {
//        case id = "_id"
//        case noOfPersons = "no_of_persons"
//        case channel
//        case statusupdateIDS = "statusupdate_ids"
//        case paymentMethod = "payment_method"
//        case isActive = "is_active"
//        case category, total
//        case isReviewed = "is_reviewed"
//        case archiveComment = "archive_comment"
//        case totalDiscount = "total_discount"
//        case totalTax = "total_tax"
//        case serivceTax = "serivce_tax"
//        case invoiceNumber = "invoice_number"
//        case billCount = "bill_count"
//        case orderNumber = "order_number"
//        case ageGroup = "age_group"
//        case discountType = "discount_type"
//        case couponID = "coupon_id"
//        case groupID = "group_id"
//        case items
//        case kotItems = "kot_items"
//        case restaurantID = "restaurant_id"
//        case serveID = "serve_id"
//        case customItems = "custom_items"
//        case updatedAt = "updated_at"
//        case createdAt = "created_at"
//        case v = "__v"
//        case itemID = "id"
//        case incomingorderRefID = "incomingorder_ref_id"
//        case userID = "user_id"
//        case totalAmount = "total_amount"
//    }
//}
//
//struct IncomingorderRefID: Codable {
//    let id: String
//    let customer: Customer
//    let order: Order
//    let accepted, isCancelled: Bool
//    let restaurantID: RestaurantID
//    let updatedAt: String
//    let v: Int
//    let orderReferenceID: String
//
//    enum CodingKeys: String, CodingKey {
//        case id = "_id"
//        case customer, order, accepted
//        case isCancelled = "is_cancelled"
//        case restaurantID = "restaurant_id"
//        case updatedAt = "updated_at"
//        case v = "__v"
//        case orderReferenceID = "order_reference_id"
//    }
//}
//
//struct Customer: Codable {
//    let address: Address
//    let phone, email, name: String
//}
//
//struct Address: Codable {
//    let city: String
//    let pin: String?
//    let longitude: Double?
//    let line1: String
//    let latitude: Double?
//    let line2, subLocality: String
//    let isGuestMode: Bool?
//    let tag: String?
//
//    enum CodingKeys: String, CodingKey {
//        case city, pin, longitude
//        case line1 = "line_1"
//        case latitude
//        case line2 = "line_2"
//        case subLocality = "sub_locality"
//        case isGuestMode = "is_guest_mode"
//        case tag
//    }
//}
//
//struct Order: Codable {
//    let details: Details
//    let store: Store
//    let nextStates: [State]
//    let items: [OrderItem]
//    let payment: [Payment]
//    let nextState: State
//
//    enum CodingKeys: String, CodingKey {
//        case details, store
//        case nextStates = "next_states"
//        case items, payment
//        case nextState = "next_state"
//    }
//}
//
//struct Details: Codable {
//    let coupon: String
//    let totalTaxes: Double
//    let merchantRefID: JSONNull?
//    let orderLevelTotalCharges: Int
//    let id: String
//    let itemLevelTotalTaxes: Double
//    let totalExternalDiscount, orderTotal: Int
//    let orderType, state, channel: String
//    let deliveryDatetime: Created
//    let itemLevelTotalCharges: Int
//    let itemTaxes: Double
//    let discount: Int
//    let orderState: State
//    let instructions: String
//    let totalCharges: Int
//    let created: Created
//    let charges: [JSONAny]
//    let extPlatforms: [EXTPlatform]
//
//    enum CodingKeys: String, CodingKey {
//        case coupon
//        case totalTaxes = "total_taxes"
//        case merchantRefID = "merchant_ref_id"
//        case orderLevelTotalCharges = "order_level_total_charges"
//        case id
//        case itemLevelTotalTaxes = "item_level_total_taxes"
//        case totalExternalDiscount = "total_external_discount"
//        case orderTotal = "order_total"
//        case orderType = "order_type"
//        case state, channel
//        case deliveryDatetime = "delivery_datetime"
//        case itemLevelTotalCharges = "item_level_total_charges"
//        case itemTaxes = "item_taxes"
//        case discount
//        case orderState = "order_state"
//        case instructions
//        case totalCharges = "total_charges"
//        case created, charges
//        case extPlatforms = "ext_platforms"
//    }
//}
//
//enum Created: Codable {
//    case integer(Int)
//    case string(String)
//
//    init(from decoder: Decoder) throws {
//        let container = try decoder.singleValueContainer()
//        if let x = try? container.decode(Int.self) {
//            self = .integer(x)
//            return
//        }
//        if let x = try? container.decode(String.self) {
//            self = .string(x)
//            return
//        }
//        throw DecodingError.typeMismatch(Created.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for Created"))
//    }
//
//    func encode(to encoder: Encoder) throws {
//        var container = encoder.singleValueContainer()
//        switch self {
//        case .integer(let x):
//            try container.encode(x)
//        case .string(let x):
//            try container.encode(x)
//        }
//    }
//}
//
//struct EXTPlatform: Codable {
//    let id, extPlatformID, kind, deliveryType: String
//    let name: String
//    let discounts: [JSONAny]
//
//    enum CodingKeys: String, CodingKey {
//        case id = "_id"
//        case extPlatformID = "id"
//        case kind
//        case deliveryType = "delivery_type"
//        case name, discounts
//    }
//}
//
//enum State: String, Codable {
//    case cancelled = "Cancelled"
//    case completed = "Completed"
//    case confirmed = "Confirmed"
//    case dispatched = "Dispatched"
//}
//
//struct OrderItem: Codable {
//    let id: String
//    let total: Int
//    let title: String
//    let charges: [JSONAny]
//    let totalWithTax: Double
//    let price, discount: Int
//    let taxes: [Tax]
//    let foodType, merchantID: String
//    let optionsToAdd: [OptionsToAdd]
//    let itemID: Created
//    let quantity: Int
//
//    enum CodingKeys: String, CodingKey {
//        case id = "_id"
//        case total, title, charges
//        case totalWithTax = "total_with_tax"
//        case price, discount, taxes
//        case foodType = "food_type"
//        case merchantID = "merchant_id"
//        case optionsToAdd = "options_to_add"
//        case itemID = "id"
//        case quantity
//    }
//}
//
//struct OptionsToAdd: Codable {
//    let id: String
//    let price: Int
//    let merchantID: MerchantID
//    let optionsToAddID: Created
//    let title: Title
//
//    enum CodingKeys: String, CodingKey {
//        case id = "_id"
//        case price
//        case merchantID = "merchant_id"
//        case optionsToAddID = "id"
//        case title
//    }
//}
//
//enum MerchantID: String, Codable {
//    case the5C7E7812D0C4Fb16Eb4933D7 = "5c7e7812d0c4fb16eb4933d7"
//    case the5C7E790F050Ee9178E08B159 = "5c7e790f050ee9178e08b159"
//}
//
//enum Title: String, Codable {
//    case cheese = "Cheese"
//    case papad = "Papad"
//}
//
//struct Tax: Codable {
//    let id: String
//    let rate: Int
//    let value: Double
//    let title: String
//
//    enum CodingKeys: String, CodingKey {
//        case id = "_id"
//        case rate, value, title
//    }
//}
//
//struct Payment: Codable {
//    let id: String
//    let amount: Int
//    let option: String
//
//    enum CodingKeys: String, CodingKey {
//        case id = "_id"
//        case amount, option
//    }
//}
//
//struct Store: Codable {
//    let name: String
//    let longitude: Double
//    let merchantRefID: RestaurantID
//    let address: String
//    let latitude: Double
//    let id: Int
//
//    enum CodingKeys: String, CodingKey {
//        case name, longitude
//        case merchantRefID = "merchant_ref_id"
//        case address, latitude, id
//    }
//}
//
//enum RestaurantID: String, Codable {
//    case the5C7E778Ed0C4Fb16Eb4933Ce = "5c7e778ed0c4fb16eb4933ce"
//}
//
//struct KotItemElement: Codable {
//    let discountAmount: Int
//    let addons: [String]
//    let comment, start, finish: JSONNull?
//    let id, itemID: String
//    let quantity: Int
//
//    enum CodingKeys: String, CodingKey {
//        case discountAmount = "discount_amount"
//        case addons, comment, start, finish
//        case id = "_id"
//        case itemID = "item_id"
//        case quantity
//    }
//}
//
//struct UserID: Codable {
//    let id: String
//    let restaurantVisited: [RestaurantID]
//    let restaurantSubscribed: [JSONAny]
//    let email: String
//    let birthday, isMarried, anniversary, gender: JSONNull?
//    let lastVisited, name, phoneNumber, createdAt: String
//    let updatedAt: String
//    let v: Int
//
//    enum CodingKeys: String, CodingKey {
//        case id = "_id"
//        case restaurantVisited = "restaurant_visited"
//        case restaurantSubscribed = "restaurant_subscribed"
//        case email, birthday, isMarried, anniversary, gender
//        case lastVisited = "last_visited"
//        case name
//        case phoneNumber = "phone_number"
//        case createdAt = "created_at"
//        case updatedAt = "updated_at"
//        case v = "__v"
//    }
//}
//
//// MARK: Encode/decode helpers
//
//class JSONNull: Codable, Hashable {
//
//    public static func == (lhs: JSONNull, rhs: JSONNull) -> Bool {
//        return true
//    }
//
//    public var hashValue: Int {
//        return 0
//    }
//
//    public init() {}
//
//    public required init(from decoder: Decoder) throws {
//        let container = try decoder.singleValueContainer()
//        if !container.decodeNil() {
//            throw DecodingError.typeMismatch(JSONNull.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for JSONNull"))
//        }
//    }
//
//    public func encode(to encoder: Encoder) throws {
//        var container = encoder.singleValueContainer()
//        try container.encodeNil()
//    }
//}
//
//class JSONCodingKey: CodingKey {
//    let key: String
//
//    required init?(intValue: Int) {
//        return nil
//    }
//
//    required init?(stringValue: String) {
//        key = stringValue
//    }
//
//    var intValue: Int? {
//        return nil
//    }
//
//    var stringValue: String {
//        return key
//    }
//}
//
//class JSONAny: Codable {
//    let value: Any
//
//    static func decodingError(forCodingPath codingPath: [CodingKey]) -> DecodingError {
//        let context = DecodingError.Context(codingPath: codingPath, debugDescription: "Cannot decode JSONAny")
//        return DecodingError.typeMismatch(JSONAny.self, context)
//    }
//
//    static func encodingError(forValue value: Any, codingPath: [CodingKey]) -> EncodingError {
//        let context = EncodingError.Context(codingPath: codingPath, debugDescription: "Cannot encode JSONAny")
//        return EncodingError.invalidValue(value, context)
//    }
//
//    static func decode(from container: SingleValueDecodingContainer) throws -> Any {
//        if let value = try? container.decode(Bool.self) {
//            return value
//        }
//        if let value = try? container.decode(Int64.self) {
//            return value
//        }
//        if let value = try? container.decode(Double.self) {
//            return value
//        }
//        if let value = try? container.decode(String.self) {
//            return value
//        }
//        if container.decodeNil() {
//            return JSONNull()
//        }
//        throw decodingError(forCodingPath: container.codingPath)
//    }
//
//    static func decode(from container: inout UnkeyedDecodingContainer) throws -> Any {
//        if let value = try? container.decode(Bool.self) {
//            return value
//        }
//        if let value = try? container.decode(Int64.self) {
//            return value
//        }
//        if let value = try? container.decode(Double.self) {
//            return value
//        }
//        if let value = try? container.decode(String.self) {
//            return value
//        }
//        if let value = try? container.decodeNil() {
//            if value {
//                return JSONNull()
//            }
//        }
//        if var container = try? container.nestedUnkeyedContainer() {
//            return try decodeArray(from: &container)
//        }
//        if var container = try? container.nestedContainer(keyedBy: JSONCodingKey.self) {
//            return try decodeDictionary(from: &container)
//        }
//        throw decodingError(forCodingPath: container.codingPath)
//    }
//
//    static func decode(from container: inout KeyedDecodingContainer<JSONCodingKey>, forKey key: JSONCodingKey) throws -> Any {
//        if let value = try? container.decode(Bool.self, forKey: key) {
//            return value
//        }
//        if let value = try? container.decode(Int64.self, forKey: key) {
//            return value
//        }
//        if let value = try? container.decode(Double.self, forKey: key) {
//            return value
//        }
//        if let value = try? container.decode(String.self, forKey: key) {
//            return value
//        }
//        if let value = try? container.decodeNil(forKey: key) {
//            if value {
//                return JSONNull()
//            }
//        }
//        if var container = try? container.nestedUnkeyedContainer(forKey: key) {
//            return try decodeArray(from: &container)
//        }
//        if var container = try? container.nestedContainer(keyedBy: JSONCodingKey.self, forKey: key) {
//            return try decodeDictionary(from: &container)
//        }
//        throw decodingError(forCodingPath: container.codingPath)
//    }
//
//    static func decodeArray(from container: inout UnkeyedDecodingContainer) throws -> [Any] {
//        var arr: [Any] = []
//        while !container.isAtEnd {
//            let value = try decode(from: &container)
//            arr.append(value)
//        }
//        return arr
//    }
//
//    static func decodeDictionary(from container: inout KeyedDecodingContainer<JSONCodingKey>) throws -> [String: Any] {
//        var dict = [String: Any]()
//        for key in container.allKeys {
//            let value = try decode(from: &container, forKey: key)
//            dict[key.stringValue] = value
//        }
//        return dict
//    }
//
//    static func encode(to container: inout UnkeyedEncodingContainer, array: [Any]) throws {
//        for value in array {
//            if let value = value as? Bool {
//                try container.encode(value)
//            } else if let value = value as? Int64 {
//                try container.encode(value)
//            } else if let value = value as? Double {
//                try container.encode(value)
//            } else if let value = value as? String {
//                try container.encode(value)
//            } else if value is JSONNull {
//                try container.encodeNil()
//            } else if let value = value as? [Any] {
//                var container = container.nestedUnkeyedContainer()
//                try encode(to: &container, array: value)
//            } else if let value = value as? [String: Any] {
//                var container = container.nestedContainer(keyedBy: JSONCodingKey.self)
//                try encode(to: &container, dictionary: value)
//            } else {
//                throw encodingError(forValue: value, codingPath: container.codingPath)
//            }
//        }
//    }
//
//    static func encode(to container: inout KeyedEncodingContainer<JSONCodingKey>, dictionary: [String: Any]) throws {
//        for (key, value) in dictionary {
//            let key = JSONCodingKey(stringValue: key)!
//            if let value = value as? Bool {
//                try container.encode(value, forKey: key)
//            } else if let value = value as? Int64 {
//                try container.encode(value, forKey: key)
//            } else if let value = value as? Double {
//                try container.encode(value, forKey: key)
//            } else if let value = value as? String {
//                try container.encode(value, forKey: key)
//            } else if value is JSONNull {
//                try container.encodeNil(forKey: key)
//            } else if let value = value as? [Any] {
//                var container = container.nestedUnkeyedContainer(forKey: key)
//                try encode(to: &container, array: value)
//            } else if let value = value as? [String: Any] {
//                var container = container.nestedContainer(keyedBy: JSONCodingKey.self, forKey: key)
//                try encode(to: &container, dictionary: value)
//            } else {
//                throw encodingError(forValue: value, codingPath: container.codingPath)
//            }
//        }
//    }
//
//    static func encode(to container: inout SingleValueEncodingContainer, value: Any) throws {
//        if let value = value as? Bool {
//            try container.encode(value)
//        } else if let value = value as? Int64 {
//            try container.encode(value)
//        } else if let value = value as? Double {
//            try container.encode(value)
//        } else if let value = value as? String {
//            try container.encode(value)
//        } else if value is JSONNull {
//            try container.encodeNil()
//        } else {
//            throw encodingError(forValue: value, codingPath: container.codingPath)
//        }
//    }
//
//    public required init(from decoder: Decoder) throws {
//        if var arrayContainer = try? decoder.unkeyedContainer() {
//            self.value = try JSONAny.decodeArray(from: &arrayContainer)
//        } else if var container = try? decoder.container(keyedBy: JSONCodingKey.self) {
//            self.value = try JSONAny.decodeDictionary(from: &container)
//        } else {
//            let container = try decoder.singleValueContainer()
//            self.value = try JSONAny.decode(from: container)
//        }
//    }
//
//    public func encode(to encoder: Encoder) throws {
//        if let arr = self.value as? [Any] {
//            var container = encoder.unkeyedContainer()
//            try JSONAny.encode(to: &container, array: arr)
//        } else if let dict = self.value as? [String: Any] {
//            var container = encoder.container(keyedBy: JSONCodingKey.self)
//            try JSONAny.encode(to: &container, dictionary: dict)
//        } else {
//            var container = encoder.singleValueContainer()
//            try JSONAny.encode(to: &container, value: self.value)
//        }
//    }
//}
