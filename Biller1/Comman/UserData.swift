
import Foundation

class UserData{
    
    //MARK: Shared Instance
    static var userInfo:UserInfo?
    static var category : [Category]?
    static var menu : [MenuElement]?
}
