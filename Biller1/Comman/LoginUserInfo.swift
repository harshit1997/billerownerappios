import Foundation


struct UserInfo:Codable {
    let name:String
    let role:String
    let otp:String
    let image: String
    let user_id:String
    let restaurant:restaurant
    let jwttoken:String
    let permissions : [String]?
}

struct restaurant:Codable {
    let _id:String?
    let address:String?
    let email : String?
    let gstin : String?
    let hsn_code :String?
    let imgname :String?
    let initials :String?
    let name:String?
    let order_count:Int?
    let phone_number1: String?
    let tin_number: Int?
    let updated_at:String?
    
}

struct Logout: Codable {
    let message: String
    let status: Int
}

struct ForgotPassword: Codable {
    let code: Int
    let status: String
    let message: String
}
