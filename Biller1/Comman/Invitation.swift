import Foundation

//typealias Invite = [InvitedUsers]

struct Invited: Codable {
    let status: Int
    let users: [InvitedUsers]
}

struct InvitedUsers: Codable {
    let id: String
    let restaurant: [String]?
    let otp: String?
    let isUserOwner, isUserWaiter, isUserManager, isUserCust: Bool?
    let permissions: [String]?
    let invitationStatus: Int
    let name, email, position, password: String?
    let phoneNumber, updatedAt, createdAt: String?

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case restaurant, otp, isUserOwner, isUserWaiter, isUserManager, isUserCust, permissions
        case invitationStatus = "invitation_status"
        case name, email, position, password
        case phoneNumber = "phone_number"
        case updatedAt = "updated_at"
        case createdAt = "created_at"
    }
}


//import Foundation
//
//struct Invite: Codable {
//    let status: Int
//    let users: [InvitedUsers]
//}
//
//struct InvitedUsers: Codable {
//    let id, jwttoken: String
//    let restaurant: [String]
//    let otp: String
//    let isUserOwner, isUserWaiter, isUserManager, isUserCust: Bool
//    let permissions: [String]
//    let invitationStatus: Int
//    let name, email, position, password: String
//    let phoneNumber, updatedAt, createdAt: String
//    let v: Int
//    
//    enum CodingKeys: String, CodingKey {
//        case id = "_id"
//        case jwttoken, restaurant, otp, isUserOwner, isUserWaiter, isUserManager, isUserCust, permissions
//        case invitationStatus = "invitation_status"
//        case name, email, position, password
//        case phoneNumber = "phone_number"
//        case updatedAt = "updated_at"
//        case createdAt = "created_at"
//        case v = "__v"
//    }
//}
