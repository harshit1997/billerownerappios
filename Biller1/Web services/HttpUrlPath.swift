//
//  HttpUrlPath.swift
//  Biller1
//
//  Created by Harshit Maheshwari on 19/02/19.
//  Copyright © 2019 Harshit Maheshwari. All rights reserved.
//

import Foundation

class HttpUrlPath{
    
    static var base_url = "https://api.mybiller.io/"
//    static var base_url = "https://restapi.mybiller.io/"
}

enum HttpsMethod{
    case Post
    case Get
    case Put
    
    var localization:String{
        switch self {
        case .Post: return "POST"
        case .Get: return "GET"
        case .Put: return "PUT"
        
        }
        
    }
}
