import UIKit
import Floaty
import MBProgressHUD

class StaffManagementViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
   
 
    
    var users:[InvitedUsers]?
    var InviteData:Invited?
    
    @IBOutlet weak var StaffTableView: UITableView!
    
    
    override func viewDidLoad() {
       super.viewDidLoad()
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        staffList()
    }
    
    
    
    func staffList(){
        
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        
        WebServices.requestHttp(partURL: "auth/invite/users", parameters: nil, method: HttpsMethod.Get, decode: { json -> Invited? in
            
            guard let response = json as? Invited else{
                return nil
            }
            
            return response
            
        }) { (response:Result) in
            DispatchQueue.main.async {
                MBProgressHUD.hide(for: self.view, animated: true)
               
                    switch response{
                    case .success(let data) :
                    self.users = data?.users
                    guard let InviteList = self.users else{
                        return
                    }
                    
                    self.StaffTableView.reloadData()
                    
                    break
        
                    case .failure(let error):
                    print(error.localizedDescription)
                    break
                    
                    }
                
                
            }
            
        }
        
       
        
    }
            
            

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let count = users?.count else{
            return 0
        }

        return count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let StaffCell = tableView.dequeueReusableCell(withIdentifier: "StaffTable", for: indexPath) as! StaffManagementTableViewCell
    StaffCell.StaffName.text = users?[indexPath.row].name
     StaffCell.StaffStatus.text = users?[indexPath.row].invitationStatus == 1 ? "Active":"Pending"
       StaffCell.StaffPosition.text = users?[indexPath.row].position
       // StaffCell.StaffName.text = "\(users?[indexPath.row].users)"
        return StaffCell
    }
    
   
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
        
            
    
    }
    
    
    
    @IBAction func ProfileBtnStaffScreen(_ sender: Any) {
        
        
        guard let profileVC = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as? UserProfile else{
            return
        }
        
        
        profileVC.modalPresentationStyle = .custom
        profileVC.view.backgroundColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.4)
        
        self.present(profileVC, animated: true, completion: nil)
        
        
    }
    
    
    
    
    
    
    
    
    
    
    
    @IBAction func AddStaffBtn(_ sender: Any) {
        
        
        
        let AddStaffVC = self.storyboard?.instantiateViewController(withIdentifier: "AddStaffController") as! AddStaffViewController
        AddStaffVC.modalPresentationStyle = .custom
       
        AddStaffVC.view.backgroundColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.4)
        
        self.present(AddStaffVC, animated: true, completion: nil)
        
    }
    }
    

