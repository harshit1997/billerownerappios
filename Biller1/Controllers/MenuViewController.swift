import UIKit
import MBProgressHUD

class MenuViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UITableViewDataSource, UITableViewDelegate {
    

    @IBOutlet weak var MenuCollectionView: UICollectionView!
    var items: [Category]?
    var availability : [Availability]?
    var itemsFinal:WelcomeItem?
    var MenuItem:MenuElement?
    var menu:[MenuElement]?
    var categoryItems = NSMutableArray()
    var arrayCat = Array<Any>()
    
    @IBOutlet weak var MenuTableView: UITableView!

    
    override func viewDidLoad() {
        super.viewDidLoad()
       
      
    }
    
    override func viewDidAppear(_ animated: Bool) {
         getMenuData() 
    }
    
    
    @IBAction func ProfileBtnMenuScreen(_ sender: Any) {
        
        guard let profileVC = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as? UserProfile else{
            return
        }
        
        
        profileVC.modalPresentationStyle = .custom
        profileVC.view.backgroundColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.4)
        
        self.present(profileVC, animated: true, completion: nil)
    }
    
    

    func getMenuData(){
       
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        WebServices.requestHttp(partURL: "menu/mget", parameters: nil, method: HttpsMethod.Get, decode: { json -> Menu? in
            
            guard let response = json as? Menu else{
                return nil
            }
            
            return response
            
        }) { (response:Result) in
            DispatchQueue.main.async {
                MBProgressHUD.hide(for: self.view, animated: true)
               
                    switch response{
                    case .success(let data) :
                    self.menu = data?.menu
                    guard let menuItems = self.menu else{
                        return
                    }
                    for item in menuItems{
                        if(!(self.categoryItems.contains(item.categoryID))){
                            self.categoryItems.add(item.categoryID)
                        }
                    }
                    self.MenuCollectionView.reloadData()
                    self.MenuTableView.reloadData()
                    break
                    case .failure(let error):
                    print(error.localizedDescription)
                    break
                    
                    }
            }
        }
        
    }
    
    
    func getMenuCategory(){
        
        MBProgressHUD.showAdded(to: self.view, animated: false)
        
    }
    

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return 4
    }
    
//    func collectionView(_ collectionView: UICollectionView, didHighlightItemAt indexPath: IndexPath) {
//        UICollectionViewCell* ceil = collectionView cellForItemAtIndexPath:indexPath
//        cell.contentView.backgroundColor = UIColor.red
//    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let MenuType = collectionView.dequeueReusableCell(withReuseIdentifier: "MenuHeadingType", for: indexPath) as! MenuCollectionViewCell
        
       // cell.UserNumber.text =  self.items?[indexPath.row].userID.phoneNumber
//MenuType.MenuPartnerName.text = self.items?.first.availability[indexPath.row].rawValue
      //  "\(self.items?[indexPath.row].orderNumber ?? 0)"
        
        MenuType.MenuPartnerName.text = "\(self.availability)"
        return MenuType
        
    }
    
    
    func updateTableView(){
        
//        let ros = self.chatTableView.numberOfRows(inSection: self.arrSections.count-1)
//        self.chatTableView.beginUpdates()
//        let index = IndexPath.init(item: ros, section: self.arrSections.count-1)
//        print(index.row)
//        self.chatTableView.insertRows(at: [index], with: .bottom)
//
//        self.chatTableView.endUpdates()
//
//        self.chatTableView.scrollToRow(at: index as IndexPath, at: .bottom, animated: true)
    }
    
    @IBAction func AddMenuItemBtn(_ sender: Any) {
        guard let AddMenuItemVC = self.storyboard?.instantiateViewController(withIdentifier: "MenuItemVC") else{
            return
        }
        AddMenuItemVC.modalPresentationStyle = .custom
        AddMenuItemVC.view.backgroundColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.4)
        
        self.present(AddMenuItemVC, animated: true, completion: nil)

        
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let itemID:String = categoryItems[section] as! String
        
        let itemInfo = UserData.menu?.filter { $0.categoryID == itemID }
        return itemInfo?.count ?? 0
       // return arrayCat.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuTable", for: indexPath)
        
        let itemID:String = categoryItems[indexPath.section] as! String
        
        let itemInfo = UserData.menu?.filter { $0.categoryID == itemID }
        
        let name = cell.viewWithTag(10) as! UILabel
        let price = cell.viewWithTag(11) as! UILabel
        name.text = itemInfo?[indexPath.row].itemName.capitalized
        price.text = "\(itemInfo?[indexPath.row].price ?? 0)"
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
       let headeView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 44))
        headeView.backgroundColor = UIColor(red: 242.00/255.00, green: 133.00/255.00, blue: 87.00/255.00, alpha: 1.00)
        let name = UILabel(frame: CGRect(x: 8, y: 0, width: 100, height: 44))
        name.textColor = UIColor.white
        name.backgroundColor = UIColor.clear

        headeView.addSubview(name)
        
        let button = UIButton()
        button.frame = headeView.bounds
        button.tag = section+1
        button.addTarget(self, action: #selector(tappedSection(sender:)), for: .touchUpInside)
        headeView.addSubview(button)
        
        let itemID:String = categoryItems[section] as! String
        
        let itemInfo = UserData.category?.filter { $0.id == itemID }
        name.text = itemInfo?.first?.name.uppercased()
        name.frame = CGRect(x: 8, y: 0, width: name.intrinsicContentSize.width, height: 44)
        return headeView
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {

        return categoryItems.count
    }
    
    @objc func tappedSection(sender:UIButton){
       
//        print(sender.tag)
//
//        let itemID = menu?[sender.tag-1].categoryID
//
//        let itemList = (UserData.menu?.filter { $0.categoryID == itemID })
//        arrayCat.append(itemList as Any)
//        let ros = self.MenuTableView.numberOfRows(inSection: (itemList?.count ?? 1)-1)
//        self.MenuTableView.beginUpdates()
//        let index = IndexPath.init(item: ros, section: (itemList?.count ?? 1 )-1)
//        print(index.row)
//        self.MenuTableView.insertRows(at: [index], with: .bottom)
//
//        self.MenuTableView.endUpdates()
//
//        self.MenuTableView.scrollToRow(at: index as IndexPath, at: .bottom, animated: true)

        
    }
    
}
