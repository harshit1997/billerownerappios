import UIKit
import MBProgressHUD

class BillViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var BillCons: NSLayoutConstraint!
    
    @IBOutlet weak var BillCollectionView: UICollectionView!
    
   
    
    var items: [WelcomeItem]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       

    }
    
    
    override func viewDidAppear(_ animated: Bool) {
         AllBtn(UIButton())
    }
    
    
    
    @IBAction func ProfileBtnBillsScreen(_ sender: Any) {
        
        
        guard let profileVC = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as? UserProfile else{
            return
        }
        
        
        profileVC.modalPresentationStyle = .custom
        profileVC.view.backgroundColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.4)
        
        self.present(profileVC, animated: true, completion: nil)
        
    }
    
    

    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let count = items?.count else{
            return 0
        }
        
        return count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let billCell = collectionView.dequeueReusableCell(withReuseIdentifier: "BillCell", for: indexPath) as! BillCollectionViewCell
        billCell.BillContactNo.text = self.items?[indexPath.row].user_id?.phoneNumber
        billCell.BillContactName.text = self.items?[indexPath.row].user_id?.name
        billCell.BillInvoiceNo.text = self.items?[indexPath.row].invoiceNumber
        billCell.BillTotalAmount.text =  "₹\(Int(self.items?[indexPath.row].totalAmount ?? 0))"
        
        return billCell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (UIScreen.main.bounds.width - 30)/2, height: 109)
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let BillItem = self.items?[indexPath.row]
        
        DispatchQueue.main.async {
            guard let BillFinalVC = self.storyboard?.instantiateViewController(withIdentifier: "BillFinalVC") as? BillFinalScreenViewController else{
                return
            }
            BillFinalVC.itemsFinal = BillItem
            BillFinalVC.modalPresentationStyle = .custom
            //BillFinalVC.view.backgroundColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.4)
            self.present(BillFinalVC, animated: true, completion: nil)
        }
        
    }

    
    @IBAction func AllBtn(_ sender: UIButton) {
        BillCons.constant = sender.frame.origin.x
        UIView.animate(withDuration: 0.4) {
            self.view.layoutIfNeeded()
            
        }
        
        

        let parameters = [
            "category": 1,
            "count": 50,
            "page": 1,
            ] as [String : Any]
        
        MBProgressHUD.showAdded(to: self.view, animated: false)
        
        
        WebServices.requestHttp(partURL: "orders/get", parameters: parameters, method: HttpsMethod.Post, decode: { json -> Items? in
            
            guard let response = json as? Items else{
                return nil
            }
            
            return response
            
        }) { (response:Result) in
            DispatchQueue.main.async {
                MBProgressHUD.hide(for: self.view, animated: true)
                switch response{
                case .success(let data) :
                    self.items = data?.items
                    self.BillCollectionView.reloadData()
                    
                    break
                    
                case .failure(let error):
                    print(error.localizedDescription)
                    break
                    
                }
                
                
            }
        }
        
       
    }
    
    
    
    
    @IBAction func Last7DaysBtn(_ sender: UIButton) {
        
        BillCons.constant = sender.frame.origin.x
        UIView.animate(withDuration: 0.4) {
            self.view.layoutIfNeeded()
            
            
            
            
            
        }
    }
    
    @IBAction func LastMonthBtn(_ sender: UIButton) {
        
        BillCons.constant = sender.frame.origin.x
        UIView.animate(withDuration: 0.4) {
            self.view.layoutIfNeeded()
            
        }
        
    }
    
    
    
    
    
    
    
    
//    @objc func billClicked(sender : UIView)
//    {
//
//        guard let BillFinalVC = self.storyboard?.instantiateViewController(withIdentifier: "BillFinalVC") else{
//            return
//        }
//        BillFinalVC.modalPresentationStyle = .custom
//        BillFinalVC.view.backgroundColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.4)
//        
//        self.present(BillFinalVC, animated: true, completion: nil)
//
//    }
//
    
    }

    

