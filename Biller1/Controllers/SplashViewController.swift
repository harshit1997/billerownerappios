//
//  SplashViewController.swift
//  Biller1
//
//  Created by Harshit Maheshwari on 15/03/19.
//  Copyright © 2019 Harshit Maheshwari. All rights reserved.
//

import UIKit

class SplashViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        let decoded  = UserDefaults.standard.object(forKey: "userInfo") as? Data
        if(decoded != nil){
            let userInfo = try! JSONDecoder.init().decode(UserInfo.self, from: decoded!)
            UserData.userInfo = userInfo
        }
        if(UserDefaults.standard.value(forKey: "userInfo") != nil){
            self.performSegue(withIdentifier: "toHome", sender: nil)
        }
        else{
            self.performSegue(withIdentifier: "toLogin", sender: nil)
        }
    }
    


}
