import UIKit

class InventoryViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
   

    override func viewDidLoad() {
        super.viewDidLoad()

    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 20
        }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let InventoryCollection = collectionView.dequeueReusableCell(withReuseIdentifier: "InventoryCollection", for: indexPath)
        return InventoryCollection
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (UIScreen.main.bounds.width - 30)/2, height: (UIScreen.main.bounds.width - 30)/2)
    }

    
    @IBAction func AddInventoryBtn(_ sender: Any) {
        
        
        
        guard let ADDInventory = self.storyboard?.instantiateViewController(withIdentifier: "AddItemVC1") else{
            return
        }
        ADDInventory.modalPresentationStyle = .custom
        ADDInventory.view.backgroundColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.4)
        
        self.present(ADDInventory, animated: true, completion: nil)

    }
    
    
    
    
    
}
