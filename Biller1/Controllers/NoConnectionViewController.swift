import UIKit

class NoConnectionViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    
    @IBAction func retryBtn(_ sender: Any) {
        
        let isNet =  WebServices.isConnectedToNetwork()
        if(isNet)
        {
            self.dismiss(animated: true, completion: nil)
        }
        else
        {
            self.showToast(message: "No internet connection")
        }
       
    }
    
    

}
