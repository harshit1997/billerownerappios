import UIKit

class BillCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var BillContactNo: UILabel!
    
    @IBOutlet weak var BillContactName: UILabel!
    
    @IBOutlet weak var BillInvoiceNo: UILabel!
    
    @IBOutlet weak var BillTotalAmount: UILabel!
    
    
}
