import UIKit
import MBProgressHUD

class AddStaffViewController: UIViewController {

    @IBOutlet weak var NameField: UITextField!
    
    @IBOutlet weak var InviteName: UITextField!
    
    @IBOutlet weak var InviteEmail: UITextField!
    
    @IBOutlet weak var InviteMobileNumber: UITextField!
    
    @IBOutlet weak var InvitePosition: UITextField!
    
    @IBOutlet weak var InviteOrdersOutlet: UIButton!
    
    @IBOutlet weak var InviteMenuOutlet: UIButton!
    
    @IBOutlet weak var InviteBillsOutlet: UIButton!
    
    @IBOutlet weak var InviteInvitesOutlet: UIButton!
    
    
    
    
    
    //var AddStaff:[Invite]?

    
    var selectedApps:NSMutableArray =  NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //InviteOrdersOutlet.backgroundColor = UIColor.gray

    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        let touch = touches.first
        let vieww = touch?.view
        
        if vieww?.tag != 10 {
            self.dismiss(animated: true, completion: nil)
        }


    }
    
   
    @IBAction func ResetallBtn(_ sender: Any) {
        
     NameField.text = nil
        InviteName.text = nil
        InviteEmail.text = nil
        InvitePosition.text = nil
        InviteMobileNumber.text = nil
        
        
    }
    
    
    
    
    @IBAction func SendInviteBtn(_ sender: Any) {
       
        if(NameField.text != nil && NameField.text!.count < 1){
        
            self.showToast(message: "Enter name")
            return
        }
        
        if(!DataValidation.isValidEmail(testStr: InviteEmail.text!)){
            
            self.showToast(message: "Invalid email")
            return
        }
        
        
//        if(!DataValidation.isValidPhoneNumber(value: InviteMobileNumber.text!)){
//           
//            self.showToast(message: "Invalid phone number")
//            return
//        }
        
        if(InvitePosition.text != nil && InvitePosition.text!.count < 1){
            
            self.showToast(message: "Enter position")
            return
        }
        
        if(selectedApps.count <= 0){
            
            self.showToast(message: "Please select option")
            return
        }
        
        
        
        
        let parameter = ["email":InviteEmail.text!,
            "position":InvitePosition.text!,
            "permissions":selectedApps,
            "name":InviteName.text!,
            "phone_number":InviteMobileNumber.text!] as [String : Any]
        
        MBProgressHUD.showAdded(to: self.view, animated: false)
        
        
        
        WebServices.requestHttp(partURL: "auth/invite", parameters: parameter, method: HttpsMethod.Post, decode: { json -> UserInfo? in
            
            guard let response = json as? UserInfo else{
                return nil
            }
            
            return response
            
        }) { (response:Result) in
            DispatchQueue.main.async {
                MBProgressHUD.hide(for: self.view, animated: true)
                
                switch(response)
                {
                case .success(let data):
                    UserData.userInfo = data
                    self.performSegue(withIdentifier: "loginSegue", sender: data)
                    break
                case .failure(let error):
                    print(error.localizedDescription)
                }
                
                
                
            }
            
        }
        
       
        
    }
    
   
    @IBAction func SelectAppsOrders(_ sender: UIButton) {
        
        if(sender.isSelected){
            sender.isSelected = false
            if(selectedApps.contains("Orders")){
            
                selectedApps.remove("Orders")
                sender.layer.borderColor = UIColor.lightGray.cgColor
                sender.layer.backgroundColor = UIColor.white.cgColor            }
        }else{
            sender.isSelected = true
            if(!selectedApps.contains("Orders")){
                selectedApps.add("Orders")
                sender.layer.borderColor = UIColor.lightGray.cgColor
                sender.layer.backgroundColor = UIColor.orange.cgColor            }
            
        }
        
      
        
    }
    
    
    
    @IBAction func SelectAppsMenu(_ sender: UIButton) {
        
        if(sender.isSelected){
            sender.isSelected = false
            if(selectedApps.contains("Menu")){
                selectedApps.remove("Menu")
                sender.layer.borderColor = UIColor.lightGray.cgColor
                sender.layer.backgroundColor = UIColor.white.cgColor
            }
        }else{
            sender.isSelected = true
            if(!selectedApps.contains("Menu")){
                selectedApps.add("Menu")
                sender.layer.borderColor = UIColor.lightGray.cgColor
                sender.layer.backgroundColor = UIColor.orange.cgColor            }
            
        }
        
        
        
    }
    
    
    @IBAction func SelectAppsBills(_ sender: UIButton) {
        
        
        if(sender.isSelected){
            sender.isSelected = false
            if(selectedApps.contains("Bills")){
                
                selectedApps.remove("Bills")
                sender.layer.borderColor = UIColor.lightGray.cgColor
                sender.layer.backgroundColor = UIColor.white.cgColor
                
            }
        }else{
            sender.isSelected = true
            if(!selectedApps.contains("Bills")){
                selectedApps.add("Bills")
                sender.layer.borderColor = UIColor.lightGray.cgColor
                sender.layer.backgroundColor = UIColor.orange.cgColor
            
            }
        }
    }
    
    
    @IBAction func SelectAppsInvites(_ sender: UIButton) {
        
        
        
        if(sender.isSelected){
            sender.isSelected = false
            if(selectedApps.contains("Invites")){
                
                selectedApps.remove("Invites")
                sender.layer.borderColor = UIColor.lightGray.cgColor
                sender.layer.backgroundColor = UIColor.white.cgColor
            }
        }else{
            sender.isSelected = true
            if(!selectedApps.contains("Invites")){
                selectedApps.add("Invites")
                sender.layer.borderColor = UIColor.lightGray.cgColor
                sender.layer.backgroundColor = UIColor.orange.cgColor
                
            }
            
        }
        
        
    }
    
    
}
