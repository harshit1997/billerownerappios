import UIKit
import Foundation
import MBProgressHUD

class HomeVC: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UITableViewDelegate, UITableViewDataSource {
  
  
   
    
    @IBOutlet weak var HomeTableView: UITableView!
    @IBOutlet weak var ComingSoon: UIView!
    @IBOutlet weak var HomeTableCellView: UIView!
    @IBOutlet weak var HomeCollectionView: UICollectionView!
    @IBOutlet weak var selectorLeadingCons: NSLayoutConstraint!
    var items: [WelcomeItem]?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        HomeCollectionView.isHidden = false
        HomeTableView.isHidden = true
        ComingSoon.isHidden = true
        
        getCategoryData()
        
        
    
    }

    override func viewDidAppear(_ animated: Bool) {
        
        btnOrder(UIButton())
    }
    
   
    
    
    
    func getCategoryData(){
        
        WebServices.requestHttp(partURL: "menu/mget", parameters: nil, method: HttpsMethod.Get, decode: { json -> Menu? in
            
            guard let response = json as? Menu else{
                return nil
            }
            
            return response
            
        }) { (response:Result) in
            DispatchQueue.main.async {
               // MBProgressHUD.hide(for: self.view, animated: true)
                switch response{
                case .success(let data) :
                    UserData.category = data?.categories
                    UserData.menu = data?.menu
                
                    break
                    
                case .failure(let error):
                    print(error.localizedDescription)
                    break
                    
                }
                
                
            }
            
        }
        
    }
   
    
    
    @IBAction func btnPrevious(_ sender: UIButton) {
        selectorLeadingCons.constant = sender.frame.origin.x
        UIView.animate(withDuration: 0.4) {
            self.view.layoutIfNeeded()
            
        }
        
        if ComingSoon.isHidden {
            ComingSoon.isHidden = false
            HomeTableView.isHidden = true
            HomeCollectionView.isHidden = true
           
            
        }
        else if ComingSoon.isHidden != true {
            ComingSoon.isHidden = false
        }
        else{
            ComingSoon.isHidden = true
        }
    }
    
    
    
    
    @IBAction func btnPendingOrders(_ sender: UIButton) {
        
//        let parameters = [
//                    "category": 0,
//                    "count": 25,
//                "page": 1,
//                ] as [String : Any]
//        
//        MBProgressHUD.showAdded(to: self.view, animated: false)
//        
//        WebServices.requestHttp(partURL: "orders/get", parameters: parameters, method: HttpsMethod.Get, decode: { json -> Items? in
//            
//            guard let response = json as? Items else{
//                return nil
//            }
//            
//            return response
//            
//        }) { (response:Result) in
//            DispatchQueue.main.async {
//                MBProgressHUD.hide(for: self.view, animated: true)
//                switch response{
//                case .success(let data) :
//                    self.items = data?.items
//                    self.HomeTableView.reloadData()
//                    
//                    break
//                    
//                case .failure(let error):
//                    print(error.localizedDescription)
//                    break
//                    
//                }
//                
//                
//            }
//        }
        
       
        
        selectorLeadingCons.constant = sender.frame.origin.x
        UIView.animate(withDuration: 0.4) {
            self.view.layoutIfNeeded()
        }
        if HomeTableView.isHidden {
            HomeTableView.isHidden = false
            HomeCollectionView.isHidden = true
            ComingSoon.isHidden = true

        }
        else if HomeTableView.isHidden != true {
            HomeTableView.isHidden = false
            }
        else{
            HomeTableView.isHidden = true
        }
        
    }
    
    
    
    
    
    @IBAction func btnOrder(_ sender: UIButton) {
        
        let parameters = [
            "category": 0,
            "count": 25,
            "page": 1,
            ] as [String : Any]
        
        MBProgressHUD.showAdded(to: self.view, animated: false)
        
        WebServices.requestHttp(partURL: "orders/get", parameters: parameters, method: HttpsMethod.Post, decode: { json -> Items? in
            
            guard let response = json as? Items else{
                return nil
            }
            
            return response
            
        }) { (response:Result) in
            DispatchQueue.main.async {
                MBProgressHUD.hide(for: self.view, animated: true)
                switch response{
                case .success(let data) :
                    self.items = data?.items
                    self.HomeCollectionView.reloadData()
                    
                    break
                    
                case .failure(let error):
                    print(error.localizedDescription)
                    break
                    
                }
                
                
            }
            
        }
        
        
        
        selectorLeadingCons.constant = sender.frame.origin.x
        UIView.animate(withDuration: 0.4) {
            self.view.layoutIfNeeded()
        }
        
        if HomeCollectionView.isHidden {
            HomeCollectionView.isHidden = false
            HomeTableView.isHidden = true
            ComingSoon.isHidden = true


        } else if HomeCollectionView.isHidden != true  {
            HomeCollectionView.isHidden = false
            }
        
        else{
            HomeCollectionView.isHidden = true

        }
    }
    
    
    //MARK:-collectionView Delegate methods
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let count = self.items?.count else {
            return 0
        }
        return count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionCell", for: indexPath) as! HomeCollectionViewCell
//        cell.tableName.text = self.items?[indexPath.row].tableNumber ?? ""
        if (items?[indexPath.row].tableNumber == nil){
            cell.tableName.text = "ORDER NO."
            cell.tableNumber.text = "\(self.items?[indexPath.row].orderNumber ?? 0)"
        }
        else {
            cell.tableNumber.text = self.items?[indexPath.row].tableNumber as? String
        }

        return cell
        
    }
    

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let count = self.items?.count else {
            return 0
        }
        return count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! HomeTableViewCell
        cell.contentView.backgroundColor = UIColor.clear
        cell.userName.text =  self.items?[indexPath.row].user_id?.name
       // cell.UserNumber.text = self.Color = UIColor.clear
        cell.UserNumber.text =  self.items?[indexPath.row].user_id?.phoneNumber
        cell.orderNumber.text =  "\(self.items?[indexPath.row].orderNumber ?? 0)"

        //cell.orderNumber.text =  self.items?[indexPath.row].userID.id
        return cell
     
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let itemInfo = self.items?[indexPath.row]
        
        DispatchQueue.main.async {
            guard let homeOrderConfirmVC = self.storyboard?.instantiateViewController(withIdentifier: "HomeOrderConfirmVC") as? HomeOrderConfirmationViewController else{
                return
            }
            homeOrderConfirmVC.itemInfo = itemInfo
            homeOrderConfirmVC.modalPresentationStyle = .custom
            homeOrderConfirmVC.view.backgroundColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.4)
            self.present(homeOrderConfirmVC, animated: true, completion: nil)
        }
       
    }
    
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        //return CGSize(width: (UIScreen.main.bounds.width - 20), height: UIScreen.main.bounds.height - 103)/4)
        return 150
    }
    
    
 

    
    @objc func buttonClicked(sender : UIView)
    {
        
        guard let HomeOrderConfirmVC = self.storyboard?.instantiateViewController(withIdentifier: "HomeOrderConfirmVC") else{
            return
        }
        HomeOrderConfirmVC.modalPresentationStyle = .custom
        HomeOrderConfirmVC.view.backgroundColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.4)

        self.present(HomeOrderConfirmVC, animated: true, completion: nil)

    }
    
    
    
    @objc func AcceptbuttonClicked(sender : UIButton){
        
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (UIScreen.main.bounds.width - 30)/2, height: (UIScreen.main.bounds.width - 30)/2)
    }
    
    @IBAction func btnPlus(_ sender: Any) {
        
        guard let addTableViewController = self.storyboard?.instantiateViewController(withIdentifier: "AddTableViewViewController") else{
            return
        }
        addTableViewController.modalPresentationStyle = .custom
        addTableViewController.view.backgroundColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.4)
       
        self.present(addTableViewController, animated: true, completion: nil)
        
    }
    
    @IBAction func BtnProfile(_ sender: Any) {
        
        guard let profileVC = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as? UserProfile else{
            return
        }
        
       
        profileVC.modalPresentationStyle = .custom
        profileVC.view.backgroundColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.4)
        
        self.present(profileVC, animated: true, completion: nil)
        
    }
    

}
