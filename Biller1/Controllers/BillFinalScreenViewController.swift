import UIKit

class BillFinalScreenViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
   
    

   var itemsFinal:WelcomeItem?
    var MenuItem:MenuElement?
    var menu:Menu?
    
    @IBOutlet weak var BillFinalTotalAmountwithoutTax: UILabel!
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    //var itemName =
    
    @IBOutlet weak var BillFinalAddress: UILabel!
    
    @IBOutlet weak var BillFinalDate: UILabel!
    
    @IBOutlet weak var BillFinalNumber: UILabel!
    
    @IBOutlet weak var BillFinalWaiter: UILabel!
    
    @IBOutlet weak var BillFinalName: UILabel!
    
    @IBOutlet weak var BillFinalMobileNumber: UILabel!
    
    @IBOutlet weak var NameOfRestaurant: UILabel!
    
    @IBOutlet weak var BillFinalRestAddress: UILabel!
    
    @IBOutlet weak var BillFinalRestNumber: UILabel!
    
    @IBOutlet weak var tableview: UITableView!
    
    @IBOutlet weak var BackBtn: UIButton!
    
    @IBOutlet weak var BillFinalTotalAmount: UILabel!
    // var itemsName:String = self
    @IBOutlet weak var BillFinalServiceTax: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        BillContents()
        
    }
    
    
    func BillContents()
    {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        let date = dateFormatter.date(from: (itemsFinal?.updatedAt)!)
        
        // change to a readable time format and change to local time zone
        if(date != nil){
            dateFormatter.dateFormat = "dd MMM yyyy, HH:MM"
            dateFormatter.timeZone = TimeZone.current
            let timeStamp = dateFormatter.string(from: date!)
            
            BillFinalDate.text = timeStamp
        }
       
        BillFinalName.text = itemsFinal?.user_id?.name
        BillFinalMobileNumber.text = itemsFinal?.user_id?.phoneNumber
        BillFinalNumber.text = itemsFinal?.invoiceNumber
        BillFinalTotalAmount.text = "₹\(Int(itemsFinal?.totalAmount ?? 0))"
//        BillFinalRestNumber.text = UserData.userInfo?.restaurant.phone_number1 to be checked
//        NameOfRestaurant.text = UserData.userInfo?.restaurant.name?.uppercased() to be checked
        BillFinalRestAddress.text = UserData.userInfo?.restaurant.address
        let tax :Float = itemsFinal?.totalTax ?? 0.0;
        BillFinalServiceTax.text = String(format:"₹%.1f",tax)
        BillFinalTotalAmountwithoutTax.text = "₹\(Int(itemsFinal?.total ?? 0))"
        
        tableview.reloadData()
        
        print(tableview.contentSize.height)
        tableViewHeight.constant = tableview.contentSize.height
        

    }
    
    @IBAction func backBtn(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
        
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        guard let count = itemsFinal?.items?.count else{
            return 0
        }
        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! BillTableCell
        cell.quantity.text =  "\(itemsFinal?.items?[indexPath.row].quantity ?? 0)"
       
       
        let itemID =  (itemsFinal?.items?[indexPath.row].itemID)!
        
        let itemInfo = UserData.menu?.filter { $0.id == itemID }
        
        cell.name.text = itemInfo?.first?.itemName
        let price : Float = itemInfo?.first?.price ?? 0.0
        let quantity : Float = Float(itemsFinal?.items?[indexPath.row].quantity ?? 0)
        cell.rate.text = String(format: "₹%.1f", price)
        cell.amount.text = String(format: "₹%.1f", (price * quantity))
       
        
        return cell
    }
    
}
