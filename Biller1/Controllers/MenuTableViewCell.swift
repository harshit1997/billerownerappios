import UIKit

class MenuTableViewCell: UITableViewCell {

    @IBOutlet weak var MenuItemsLabel: UILabel!
    @IBOutlet weak var MenuItemsTableView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
