
import UIKit

class LeftMenuViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    
    
    
    
    
    private let titlesArray = ["Order",
//                               "CRM",
                               "Bills",
//                               "Inventory",
                               "Menu",
                               "Staff Management"]
//                               "Cash Management"]
    
    
    @IBOutlet weak var OfficeAddressLbl: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    let MenuImages = [UIImage(named: "order"), UIImage(named: "bills"), UIImage(named: "menu-1"), UIImage(named: "crm")]
    
    override func viewDidLoad() {
        super.viewDidLoad()
       tableView.contentInset = UIEdgeInsets(top: 44.0, left: 0.0, bottom: 44.0, right: 0.0)
        self.view.backgroundColor = .clear
        
        OfficeAddressLbl.text = UserData.userInfo?.restaurant.address
        OfficeAddressLbl.font = UIFont(name: "Comfortaa-Regular", size: 40)

        
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override var preferredStatusBarUpdateAnimation: UIStatusBarAnimation {
        return .fade
    }
    
    // MARK: - UITableViewDataSource
    
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titlesArray.count
    }
    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! LeftViewCell
        
        cell.titleLabel.text = titlesArray[indexPath.row]
        cell.isUserInteractionEnabled = (indexPath.row != 4 )//&& indexPath.row != 3)
        cell.titleLabel.font = UIFont(name: "Comfortaa-Regular", size: 45)
        cell.MenuIcons.contentMode = .scaleAspectFit
        cell.MenuIcons.image = self.MenuImages[indexPath.row]
        return cell
    }
    
    // MARK: - UITableViewDelegate
    
     func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return  64.0
    }
    
     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let mainViewController = sideMenuController!
        let navigationController = mainViewController.rootViewController as! NavigationController
        let viewController: UIViewController!
        
        if indexPath.row == 0 {

            
            viewController = self.storyboard!.instantiateViewController(withIdentifier: "ViewController")
            navigationController.pushViewController(viewController, animated: true)
            mainViewController.hideLeftView(animated: true, delay: 0.0, completionHandler: nil)
        }
        else if indexPath.row == 1 {
            
           
            viewController = self.storyboard!.instantiateViewController(withIdentifier: "BillViewController")
            navigationController.pushViewController(viewController, animated: true)
            mainViewController.hideLeftView(animated: true, delay: 0.0, completionHandler: nil)
            
        }
//        else if indexPath.row == 3{
//
//            viewController = self.storyboard!.instantiateViewController(withIdentifier: "InventoryViewController")
//            navigationController.pushViewController(viewController, animated: true)
//            mainViewController.hideLeftView(animated: true, delay: 0.0, completionHandler: nil)
        
       // }
        else if indexPath.row == 2{
            
            viewController = self.storyboard!.instantiateViewController(withIdentifier: "MenuViewController")
            navigationController.pushViewController(viewController, animated: true)
            mainViewController.hideLeftView(animated: true, delay: 0.0, completionHandler: nil)
            
        }
        else if indexPath.row == 3{
            
            viewController = self.storyboard!.instantiateViewController(withIdentifier: "StaffManagementViewController")
            navigationController.pushViewController(viewController, animated: true)
            mainViewController.hideLeftView(animated: true, delay: 0.0, completionHandler: nil)
            
//        }
//        else if indexPath.row == 6{
//
//            viewController = self.storyboard!.instantiateViewController(withIdentifier: "CashManagementViewController")
//            navigationController.pushViewController(viewController, animated: true)
//            mainViewController.hideLeftView(animated: true, delay: 0.0, completionHandler: nil)
//
//        }
//        else {
//            let viewController = UIViewController()
//            viewController.view.backgroundColor = .white
//            viewController.title = "Test \(titlesArray[indexPath.row])"
//
//            let navigationController = mainViewController.rootViewController as! NavigationController
//            navigationController.pushViewController(viewController, animated: true)
//
//            mainViewController.hideLeftView(animated: true, completionHandler: nil)
//        }
    }
  

}
}
