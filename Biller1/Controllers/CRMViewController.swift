import UIKit

class CRMViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    

    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 30
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let CRMCell = collectionView.dequeueReusableCell(withReuseIdentifier: "CRMCell", for: indexPath)
        return CRMCell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (UIScreen.main.bounds.width - 30)/2, height: (UIScreen.main.bounds.width - 30)/2)
    }

    @IBAction func CRMAddBtn(_ sender: Any) {
        
        guard let addTableViewController = self.storyboard?.instantiateViewController(withIdentifier: "CRMADDED") else{
            return
        }
        addTableViewController.modalPresentationStyle = .custom
        addTableViewController.view.backgroundColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.4)
        
        self.present(addTableViewController, animated: true, completion: nil)
        
        
        
    }
    

}
