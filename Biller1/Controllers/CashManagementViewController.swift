import UIKit
import Floaty

class CashManagementViewController: UIViewController,UITableViewDelegate, UITableViewDataSource {
   

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let floaty = Floaty()
        floaty.addItem(title: "Add Cash")
        floaty.addItem(title: "Withdraw Cash")
        self.view.addSubview(floaty)
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 20
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cashCell = tableView.dequeueReusableCell(withIdentifier: "CashCell", for: indexPath)
        return cashCell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70.0
    }
   

}
