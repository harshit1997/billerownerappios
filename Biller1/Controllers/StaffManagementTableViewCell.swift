import UIKit

class StaffManagementTableViewCell: UITableViewCell {

    @IBOutlet weak var StaffStatus: UILabel!
    @IBOutlet weak var StaffPosition: UILabel!
    @IBOutlet weak var StaffName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
