import UIKit



class HomeOrderConfirmationViewController: UIViewController {

    @IBOutlet weak var UserNameOrderConfirmation: UILabel!
    @IBOutlet weak var SummaryView: UIView!
    var itemInfo:WelcomeItem?
    
    @IBOutlet weak var InvoiceDateOrderConfirmation: UILabel!
    @IBOutlet weak var InvoiceNumberOrderConfirmation: UILabel!
    @IBOutlet weak var UserMobileNumberOrderConfirmation: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        SummaryView.layer.masksToBounds = false
        SummaryView.layer.cornerRadius = 2.0
        SummaryView.layer.shadowOffset = CGSize(width: -1, height: 1)
        SummaryView.layer.shadowOpacity = 0.2
         setupItemInfo()
       
        
    }
    
    
    func setupItemInfo(){
        UserNameOrderConfirmation.text = itemInfo?.user_id?.name
        UserMobileNumberOrderConfirmation.text = itemInfo?.user_id?.phoneNumber
        InvoiceNumberOrderConfirmation.text = itemInfo?.invoiceNumber
        InvoiceDateOrderConfirmation.text = itemInfo?.updatedAt



    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        let touch = touches.first
        let vieww = touch?.view
        
        if vieww?.tag != 11 
        {
            self.dismiss(animated: true, completion: nil)
        }
        
    }
    

   

}
