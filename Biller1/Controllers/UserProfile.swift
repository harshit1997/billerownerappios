import UIKit
import MBProgressHUD

class UserProfile: UIViewController {

    @IBOutlet weak var EmailLbl: UILabel!
    
    @IBOutlet weak var EmailTxtFieldHeightCons: NSLayoutConstraint!
    @IBOutlet weak var EmailHeightCons: NSLayoutConstraint!
    @IBOutlet weak var UserProfileViewHeightCons: NSLayoutConstraint!
    @IBOutlet weak var EmailTopCons: NSLayoutConstraint!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userContactNumber: UILabel!
    @IBOutlet weak var userEmail: UILabel!
    @IBOutlet weak var UserInitialsName: UILabel!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.setUserInfo()
    }
    
    
    private func setUserInfo(){
      
        let stringInput = UserData.userInfo?.name ?? ""
        let stringInputArr = stringInput.components(separatedBy: " ")
        var stringNeed = ""
        
        for string in stringInputArr {
            stringNeed = stringNeed + String(string.first!)
        }
        
        UserInitialsName.text = stringNeed.uppercased()
        userName.text = UserData.userInfo?.name.uppercased()
        userContactNumber.text = UserData.userInfo?.restaurant.phone_number1
        userEmail.text = UserData.userInfo?.restaurant.email
         //userName.text = UserData.userInfo?.restaurant.address
        
        if userEmail.text == nil{
            EmailLbl.isHidden = true
            EmailTopCons.constant = 0
            UserProfileViewHeightCons.constant -= 90
            EmailHeightCons.constant = 0
            EmailTxtFieldHeightCons.constant = 0
            
        }
        if userContactNumber.text == nil{
            userContactNumber.isHidden = true
            EmailTopCons.constant = 0
            UserProfileViewHeightCons.constant -= 90
            EmailHeightCons.constant = 0
            EmailTxtFieldHeightCons.constant = 0
            
        }
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        let touch = touches.first
        let vieww = touch?.view
        
       
        if vieww?.tag != 10 &&  vieww?.tag != 11 {
            self.dismiss(animated: true, completion: nil)
        }
        
    }
    
    
    @IBAction func LogOutBtn(_ sender: Any) {

        MBProgressHUD.showAdded(to: self.view, animated: false)
        
        WebServices.requestHttp(partURL: "auth/logout", parameters: nil, method: HttpsMethod.Post, decode: { json -> Logout? in
            
            guard let response = json as? Logout else{
                return nil
            }
            
            return response
            
        }) { (response:Result) in
            DispatchQueue.main.async {
                MBProgressHUD.hide(for: self.view, animated: true)
                switch response{
                case .success(let data) :
                   
                    UserDefaults.standard.setValue(nil, forKeyPath: "userInfo")
                    UserDefaults.standard.synchronize()
                    self.performSegue(withIdentifier: "toLoginFromProfile", sender: nil)
//                    self.dismiss(animated: true, completion: nil)
//                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dismissToRootVC"), object: nil, userInfo: nil)
                    
                    break
                    
                case .failure(let error):
                    print(error.localizedDescription)
                    break
                    
                }
                
                
            }
            
        }
        
        
    }
    
    

}
